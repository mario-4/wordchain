package com.task;

import com.task.chain.BeginEndPair;
import com.task.chain.WordChainBuilder;
import com.task.dictionary.QuadraticWordDictionary;
import com.task.dictionary.WordDictionary;
import com.task.dictionary.WordFilter;
import com.task.matcher.WordMatcher;

import java.util.Scanner;

public class Main {


    public static void main(String[] args) {

        WordMatcher wordMatcher = new WordMatcher();
        WordFilter lineProcessor = new WordFilter();
        WordDictionary wordDictionary = new QuadraticWordDictionary(wordMatcher, lineProcessor);

        WordChainBuilder wordChainBuilder = new WordChainBuilder(wordDictionary, wordMatcher);

        Scanner reader = new Scanner(System.in);  // Reading from System.in
        System.out.println("Enter the initial and final word pair: ");
        BeginEndPair beginEndPair = new BeginEndPair(reader.nextLine(), reader.nextLine());

        lineProcessor.setWordLenght(beginEndPair.getInitialWord().length());
        wordDictionary.init();

        if (wordMatcher.matches(beginEndPair.getInitialWord(), beginEndPair.getFinalWord())) {
            System.out.println(beginEndPair.getInitialWord() + " " + beginEndPair.getFinalWord());
        } else  {
            System.out.println(wordChainBuilder.buildShortestWordChain(beginEndPair));
        }
    }
}
