package com.task.chain;

public class ChainMember {

    private boolean isLeaf;
    private String word;

    public ChainMember(boolean isLeaf, String word) {
        this.isLeaf = isLeaf;
        this.word = word;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ChainMember that = (ChainMember) o;

        if (isLeaf != that.isLeaf) return false;
        return word.equals(that.word);
    }

    @Override
    public int hashCode() {
        int result = (isLeaf ? 1 : 0);
        result = 31 * result + word.hashCode();
        return result;
    }

    public boolean isLeaf() {
        return isLeaf;
    }

    public void setLeaf(boolean leaf) {
        this.isLeaf = leaf;
    }

    public String getWord() {
        return word;
    }
}
