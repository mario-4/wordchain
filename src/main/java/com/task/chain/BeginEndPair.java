package com.task.chain;

import static java.util.Objects.requireNonNull;

public class BeginEndPair {

    private String initialWord;
    private String finalWord;

    public BeginEndPair(String initialWord, String finalWord) {
        this.initialWord = requireNonNull(initialWord,"initialWord should be supplied");
        this.finalWord = requireNonNull(finalWord,"initialWord should be supplied");;
    }

    public String getInitialWord() {
        return initialWord;
    }

    public void setInitialWord(String initialWord) {
        this.initialWord = initialWord;
    }

    public String getFinalWord() {
        return finalWord;
    }

    public void setFinalWord(String finalWord) {
        this.finalWord = finalWord;
    }

    public boolean isValid(){
        return initialWord.length()==finalWord.length();
    }
}
