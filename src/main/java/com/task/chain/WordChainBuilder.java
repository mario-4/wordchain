package com.task.chain;

import com.scalified.tree.TreeNode;
import com.scalified.tree.multinode.ArrayMultiTreeNode;
import com.scalified.tree.multinode.MultiTreeNode;
import com.task.dictionary.WordDictionary;
import com.task.matcher.WordMatcher;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

public class WordChainBuilder {

    private final WordDictionary<List<String>, String> wordDictionary;
    private final WordMatcher wordMatcher;
    private MultiTreeNode<ChainMember> treeOfChainsRoot;

    public WordChainBuilder(WordDictionary<List<String>, String> wordDictionary, WordMatcher wordMatcher) {
        this.wordDictionary = requireNonNull(wordDictionary, "wordDictionary should be supplied");
        this.wordMatcher = requireNonNull(wordMatcher, "wordMatcher should be supplied");
    }

    public String buildShortestWordChain(BeginEndPair beginEndPair) {
        if (!beginEndPair.isValid()) {
            throw new IllegalStateException("The words are not compatible - They should have same length");
        }

        treeOfChainsRoot = new ArrayMultiTreeNode(new ChainMember(false, beginEndPair.getInitialWord()));

        MultiTreeNode<ChainMember> lastWordOfShortestChain = buildTreeOfWordChains(beginEndPair, treeOfChainsRoot);

        StringBuilder chain = new StringBuilder("");

        Optional.ofNullable(lastWordOfShortestChain).ifPresent((node) -> {
            for (TreeNode<ChainMember> member : treeOfChainsRoot.path(node)) {
                chain.append(member.data().getWord()).append(" ");
            }
            chain.append(beginEndPair.getFinalWord());
        });

        return chain.toString();
    }

    private MultiTreeNode<ChainMember> buildTreeOfWordChains(BeginEndPair beginEndPair, MultiTreeNode<ChainMember> currentNode) {

        Optional<MultiTreeNode<ChainMember>> newChainLevel = Optional.ofNullable(null);
        LinkedList<TreeNode<ChainMember>> unmatchedLeafWords = new LinkedList<>();
        PostOrderedLeafList postTraversalAction = new PostOrderedLeafList(unmatchedLeafWords);

        if (!currentNode.isRoot()) {
            currentNode.root().traversePostOrder(postTraversalAction);
        }

        unmatchedLeafWords.addFirst(currentNode);

        for (TreeNode<ChainMember> chainMember : unmatchedLeafWords) {
            for (String word : wordDictionary.getNextWordsBasedOn(chainMember.data().getWord())) {
                MultiTreeNode<ChainMember> newNode = new ArrayMultiTreeNode<>(new ChainMember(true, word));
                if (!newChainLevel.isPresent()) {
                    newChainLevel = Optional.of(newNode);
                }
                chainMember.add(newNode);
                if (wordMatcher.matches(word, beginEndPair.getFinalWord())) {
                    newNode.data().setLeaf(true);
                    return newNode;
                }
            }
        }
        if (!newChainLevel.isPresent())
            return null;
        currentNode.data().setLeaf(false);
        return buildTreeOfWordChains(beginEndPair, newChainLevel.get());
    }

}
