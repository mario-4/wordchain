package com.task.chain;

import com.scalified.tree.TraversalAction;
import com.scalified.tree.TreeNode;

import java.util.LinkedList;

public class PostOrderedLeafList implements TraversalAction<TreeNode<ChainMember>> {


    private LinkedList<TreeNode<ChainMember>> linkedList;

    public PostOrderedLeafList(LinkedList linkedList){
        this.linkedList=linkedList;
    }

    @Override
    public void perform(TreeNode<ChainMember> node) {
        if(node.data().isLeaf()){
            linkedList.addFirst(node);
        }
    }

    @Override
    public boolean isCompleted() {
        return false;
    }
}
