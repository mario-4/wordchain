package com.task.matcher;

public class WordMatcher {

    public boolean matches(String current, String nextWord) {
        char[] c = current.toLowerCase().toCharArray();
        char[] n = nextWord.toLowerCase().toCharArray();
        if (c.length != n.length) return false;
        int differentChars = 0;
        for (int i = 0; i < c.length; i++) {
            if (!(n[i] == c[i])) {
                differentChars++;
            }
        }
        return differentChars == 1 ? true : false;
    }
}
