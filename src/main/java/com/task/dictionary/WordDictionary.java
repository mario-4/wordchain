package com.task.dictionary;

public interface WordDictionary<R,T> {

    void init();

    R getNextWordsBasedOn(T current);

}
