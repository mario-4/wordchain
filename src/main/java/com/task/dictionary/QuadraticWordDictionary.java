package com.task.dictionary;

import com.google.common.io.CharSource;
import com.google.common.io.LineProcessor;
import com.task.matcher.WordMatcher;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import static java.util.Objects.requireNonNull;

public class QuadraticWordDictionary implements WordDictionary<List<String>, String> {

    private CharSource charSource;
    private final WordMatcher wordMatcher;
    private final LineProcessor<List<String>> lineProcessor;

    public QuadraticWordDictionary(WordMatcher wordMatcher, LineProcessor<List<String>> lineProcessor) {
        this.wordMatcher = requireNonNull(wordMatcher,"wordMatcher should be supplied");
        this.lineProcessor = requireNonNull(lineProcessor,"lineProcessor should be supplied");
    }

    @Override
    public void init() {
        charSource = DictionaryLoader.loadDefaultWordList();
        try {
            charSource.readLines(lineProcessor);
            lineProcessor.getResult();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<String> getNextWordsBasedOn(String current) {

        List<String> nextWords = new ArrayList<>();
        for (ListIterator<String> it = lineProcessor.getResult().listIterator(); it.hasNext(); ) {
            String word = it.next();
           if(wordMatcher.matches(current, word)){
               nextWords.add(word);
               it.remove();
           }
        }

        return nextWords;
    }
}