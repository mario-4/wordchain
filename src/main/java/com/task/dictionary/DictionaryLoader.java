package com.task.dictionary;

import com.google.common.base.Charsets;
import com.google.common.io.CharSource;

import static com.google.common.io.Resources.asCharSource;
import static com.google.common.io.Resources.getResource;

public final class DictionaryLoader {

    private static final String DEFAULT_WORDLIST = "wordlist.txt";

    private DictionaryLoader() {

    }

    public static CharSource loadDefaultWordList() {
        return asCharSource(getResource(DEFAULT_WORDLIST), Charsets.UTF_8);
    }

    public static CharSource loadCustomWordList(String customWordList) {
        return asCharSource(getResource(customWordList), Charsets.UTF_8);
    }
}
