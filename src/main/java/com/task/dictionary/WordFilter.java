package com.task.dictionary;

import com.google.common.io.LineProcessor;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WordFilter implements LineProcessor<List<String>> {

    private int wordLenght;

    private List<String> filteredWords;

    public WordFilter(){
        this.filteredWords = new ArrayList<>();
    }

    public WordFilter(int wordLenght) {
        this.wordLenght = wordLenght;
        this.filteredWords = new ArrayList<>();
    }

    @Override
    public boolean processLine(String line) throws IOException {
        if (line.length() == wordLenght) {
            filteredWords.add(line);
        }
        return true;
    }

    @Override
    public List<String> getResult() {
        return filteredWords;
    }


    public void setWordLenght(int wordLenght) {
        this.wordLenght = wordLenght;
    }
}
