package com.task.matcher;

import com.task.matcher.WordMatcher;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

public class WordMatcherTest {


    private WordMatcher wordMatcher;

    @Before
    public void setUp() {
        wordMatcher = new WordMatcher();
    }

    @Test
    public void shouldMatchDifferentWordsByOneChar() {
        Assertions.assertThat(wordMatcher.matches("cat", "cot")).isEqualTo(true);
    }

    @Test
    public void shouldNotMatchDifferentWordsByMoreThanOneChar() {
        Assertions.assertThat(wordMatcher.matches("cai", "cot")).isEqualTo(false);
    }

    @Test
    public void shouldNotMatchWhenCharsInDifferentOrder() {
        Assertions.assertThat(wordMatcher.matches("cai", "iac")).isEqualTo(false);
    }

    @Test
    public void shouldNotMatchWhenWordsHaveDifferentLenghts() {
        Assertions.assertThat(wordMatcher.matches("cai", "caii")).isEqualTo(false);
    }

    @Test
    public void shouldNotIgnoreCaseOnMatchingCriteria() {
        Assertions.assertThat(wordMatcher.matches("cai", "Cai")).isEqualTo(false);
    }
}