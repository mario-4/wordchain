package com.task.chain;

import com.task.dictionary.WordDictionary;
import com.task.matcher.WordMatcher;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.mockito.BDDMockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WordChainBuilderTest {

    @Mock
    private WordDictionary<List<String>, String> dictionary;

    @Mock
    private WordMatcher wordMatcher;

    private WordChainBuilder wordChainBuilder;

    @Before
    public void setUp() {
        wordChainBuilder = new WordChainBuilder(dictionary,wordMatcher);
    }

    @Test
    public void shouldGetShortestPathInWordChain() {
        when(dictionary.getNextWordsBasedOn("cat")).thenReturn(Stream.of("cot", "cai","dat","vat").collect(Collectors.toList()));
        when(dictionary.getNextWordsBasedOn("cot")).thenReturn(Stream.of("cog").collect(Collectors.toList()));

        when(wordMatcher.matches("cog","dog")).thenReturn(true);

        BeginEndPair beginEndPair = new BeginEndPair("cat", "dog");

        Assertions.assertThat(wordChainBuilder.buildShortestWordChain(beginEndPair)).isEqualTo("cat cot cog dog");
    }

    @Test
    public void shouldGetEmptyIfChainIsNotFound() {
        when(dictionary.getNextWordsBasedOn("cat")).thenReturn(Stream.of("cot", "cai","dat").collect(Collectors.toList()));
        when(dictionary.getNextWordsBasedOn("cot")).thenReturn(Collections.emptyList());
        when(dictionary.getNextWordsBasedOn("cai")).thenReturn(Collections.emptyList());
        when(dictionary.getNextWordsBasedOn("dat")).thenReturn(Collections.emptyList());

        BeginEndPair beginEndPair = new BeginEndPair("cat", "dog");

        Assertions.assertThat(wordChainBuilder.buildShortestWordChain(beginEndPair)).isEqualTo("");
    }

    @Test(expected = IllegalStateException.class)
    public void shouldGetInitialAndFinalWordInBaseCase() {
        BeginEndPair beginEndPair = new BeginEndPair("cat", "caia");

        wordChainBuilder.buildShortestWordChain(beginEndPair);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionIfDictionaryIsNull() {
        new WordChainBuilder(null, wordMatcher);
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionIfWordMAtcherIsNull() {
        new WordChainBuilder(dictionary, null);
    }
}
