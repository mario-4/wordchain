package com.task.chain;

import com.task.chain.BeginEndPair;
import org.assertj.core.api.Assertions;
import org.junit.Test;

public class BeginEndPairTest {

    public void setUp(){

    }

    @Test
    public void shouldHavePairEqualInSize(){
        BeginEndPair beginEndPair=new BeginEndPair("cat","cot");
        Assertions.assertThat(beginEndPair.isValid()).isTrue();
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenInitialWordIsNull(){
        BeginEndPair beginEndPair=new BeginEndPair(null,"cot");
    }

    @Test(expected = NullPointerException.class)
    public void shouldThrowNullPointerExceptionWhenFinalWordIsNull(){
        BeginEndPair beginEndPair=new BeginEndPair("cat",null);
        Assertions.assertThat(beginEndPair.isValid()).isTrue();
    }

}
