package com.task.dictionary;

import com.google.common.io.LineProcessor;
import com.task.dictionary.QuadraticWordDictionary;
import com.task.matcher.WordMatcher;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.BDDMockito.when;

@RunWith(MockitoJUnitRunner.class)
public class QuadraticDictionaryTest {


    @Mock
    private LineProcessor<List<String>> lineProcessor;

    @Mock
    private WordMatcher wordMatcher;


    private QuadraticWordDictionary quadraticDictionary;

    @Before
    public void setUp() {
        quadraticDictionary = new QuadraticWordDictionary(wordMatcher, lineProcessor);
    }

    @Test
    public void shouldGetNextWordsUsingWordMatcherAndLineProcessor() {
        List<String> processedLines = new ArrayList<>();
        processedLines.add("cai");
        processedLines.add("col");
        processedLines.add("cal");
        processedLines.add("val");
        when(lineProcessor.getResult()).thenReturn(processedLines);
        when(wordMatcher.matches("cat", "cai")).thenReturn(true);
        when(wordMatcher.matches("cat", "cal")).thenReturn(true);
        Assertions.assertThat(quadraticDictionary.getNextWordsBasedOn("cat")).containsExactly("cai", "cal");
    }

    @Test
    public void shouldNotGetWordsWhenThereIsNoMatch() {
        List<String> processedLines = new ArrayList<>();
        processedLines.add("cui");
        processedLines.add("col");
        processedLines.add("coe");
        processedLines.add("val");
        when(lineProcessor.getResult()).thenReturn(processedLines);
        Assertions.assertThat(quadraticDictionary.getNextWordsBasedOn("cat")).isEmpty();
    }

    @Test(expected = NullPointerException.class)
    public void shoulThrowNullPointerExceptionForNullWordMatcher() {
        quadraticDictionary = new QuadraticWordDictionary(null, lineProcessor);

    }

    @Test(expected = NullPointerException.class)
    public void shoulThrowNullPointerExceptionForNullLineProcessor() {
        quadraticDictionary = new QuadraticWordDictionary(wordMatcher, null);
    }
}
