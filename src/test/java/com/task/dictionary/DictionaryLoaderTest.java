package com.task.dictionary;

import com.task.dictionary.DictionaryLoader;
import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static com.google.common.io.Resources.getResource;

public class DictionaryLoaderTest {

    @Before
    public void setUp(){}

    @Test
    public void shouldLoadNonEmptyWordList() throws IOException {
        Assertions.assertThat(DictionaryLoader.loadDefaultWordList().isEmpty()).isFalse();
    }

    @Test
    public void shouldLoadNonEmptyCustomWordList() throws IOException{
        Assertions.assertThat(DictionaryLoader.loadCustomWordList("wordlist.txt").isEmpty()).isFalse();
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowIllegalArgumentExceptionWhenCustomWordListDoesNotExist(){
        DictionaryLoader.loadCustomWordList("wordlist2.txt");
    }
}
