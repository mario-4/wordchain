# README #

Coding Task - Mario Paulo Silva Soares

### What is this repository for? ###

* Coding task
* Version 1.0
* http://codekata.com/kata/kata19-word-chains/

### How do I get set up? ###

* Build the project with a simple gradle build
* Execute program from Main class
* If you decide to to pull the branch with the application plugin, use gradle installDist and run the application using the genrated scripts 